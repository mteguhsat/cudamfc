varying vec3 normal;
varying vec3 position;

void main()
{	
	gl_Position = ftransform();
	normal = normalize(gl_NormalMatrix * gl_Normal);
    position = vec3(gl_ModelViewMatrix * gl_Vertex);  

} 

