#pragma once

#include <vector>
#include "cudaMFC10View.h"
#include "GLRenderer.h"

using namespace std;
// CwhiteBoard view

class CwhiteBoard : public CcudaMFC10View
{
	DECLARE_DYNCREATE(CwhiteBoard)

protected:
	CwhiteBoard();           // protected constructor used by dynamic creation
	virtual ~CwhiteBoard();

public:
	void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:

protected:
	CGLRenderer m_glRenderer;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void OnInitialUpdate();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
public:

};


