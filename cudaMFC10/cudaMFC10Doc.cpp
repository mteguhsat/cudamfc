// cudaMFC10Doc.cpp : implementation of the CcudaMFC10Doc class
//

#include "stdafx.h"
#include "cudaMFC10.h"

#include "cudaMFC10Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CcudaMFC10Doc

IMPLEMENT_DYNCREATE(CcudaMFC10Doc, CDocument)

BEGIN_MESSAGE_MAP(CcudaMFC10Doc, CDocument)
	ON_COMMAND(ID_FILE_SAVE_AS, &CcudaMFC10Doc::OnFileSaveAs)
	ON_COMMAND(ID_FILE_SAVE, &CcudaMFC10Doc::OnFileSave)
	ON_COMMAND(ID_FILE_OPEN, &CcudaMFC10Doc::OnFileOpen)
END_MESSAGE_MAP()


// CcudaMFC10Doc construction/destruction

CcudaMFC10Doc::CcudaMFC10Doc()
: contentText(_T(""))
{
	// TODO: add one-time construction code here
	className = "class CcudaMFC10Doc";

}

CcudaMFC10Doc::~CcudaMFC10Doc()
{
	cudaTesting.clear();
	GPUprop.clear();
	dataHeader.clear();
	dataBody.clear();
}

BOOL CcudaMFC10Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

// CcudaMFC10Doc serialization

void CcudaMFC10Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring()) // save data
	{
		// TODO: add storing code here
	}
	else	//load data
	{
		// TODO: add loading code here
		CString temp;
		string str;
		//ar >> str;
		temp.Format("%s+%s+%s",folderPath,fileName,fileExt);
		MessageBox(NULL,temp, "Warning", MB_OK | MB_ICONWARNING);
	}
}


// CcudaMFC10Doc diagnostics

#ifdef _DEBUG
void CcudaMFC10Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CcudaMFC10Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CcudaMFC10Doc commands

void CcudaMFC10Doc::OnFileSaveAs()
{
	//CFile flSaveAs;
	LPCTSTR pszFilter = 
		_T("Text files (*.txt;*.dat)|*.txt;*.dat|")
		_T("All files (*.*)|*.*||");
	
	CFileDialog dlgFile(FALSE, ".txt", "savedfile", OFN_OVERWRITEPROMPT,
                       pszFilter, AfxGetMainWnd());
	
	if(IDOK == dlgFile.DoModal())
	{
		fileName  = dlgFile.GetFileName();
		folderPath= dlgFile.GetFolderPath();
		fileExt	  = dlgFile.GetFileExt();
		pathName  = dlgFile.GetPathName();
		mfileHandler.setSavedFileName(pathName);
		mfileHandler.saveDataToFile(&dataHeader, &dataBody);
	}else
		return;
}

void CcudaMFC10Doc::OnFileSave()
{
	CFile flSave;

	if (!pathName.IsEmpty()){
		mfileHandler.setSavedFileName(pathName);
		mfileHandler.saveDataToFile(&dataHeader, &dataBody);
	}else
		OnFileSaveAs();
}

void CcudaMFC10Doc::OnFileOpen()
{
	LPCTSTR pszFilter = 
		_T("Text files (*.txt;*.dat)|*.txt;*.dat|")
		_T("All files (*.*)|*.*||");
	
	CFileDialog dlgFile(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST,
                       pszFilter, AfxGetMainWnd());

	if(IDOK == dlgFile.DoModal())
	{
		fileName  = dlgFile.GetFileName();
		folderPath= dlgFile.GetFolderPath();
		fileExt	  = dlgFile.GetFileExt();
		pathName  = dlgFile.GetPathName();
		mfileHandler.setDataFile(pathName);
		mfileHandler.parseDataFile(&dataHeader, &dataBody);
	}else
		return;
}

