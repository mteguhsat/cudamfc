#pragma once

#include "cudaMFC10Doc.h"
#include "cudaMFC10View.h"
#include "whiteBoard.h"
#include "mainCuda.h"
#include "afxwin.h"

// CsideBar form view

class CsideBar : public CFormView
{
	DECLARE_DYNCREATE(CsideBar)

protected:
	CsideBar();           // protected constructor used by dynamic creation
	virtual ~CsideBar();

public:
	enum { IDD = IDD_SIDEBAR };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CmainCuda m_mainCuda;
	afx_msg void OnBnClickedBstart();
	CListBox mlisBox;
};


