#pragma once
// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes, project
//#include <cutil_inline.h>

// Required header to support CUDA vector types
#include <vector_types.h>
#include <cufft.h>

__global__ void kernel( int* g_data );
__global__ void kernel2( int2* g_data );
__global__ void landKernel(float4* pos, unsigned int width, 
							  unsigned int height);
#define cutilSafeCall(err)           __cudaSafeCall      (err, __FILE__, __LINE__)
#define cutilCheckMsg(msg)           __cutilCheckMsg     (msg, __FILE__, __LINE__)
inline void __cudaSafeCall( cudaError err, const char *file, const int line )
{
    if( cudaSuccess != err) {
        fprintf(stderr, "cudaSafeCall() Runtime API error in file <%s>, line %i : %s.\n",
                file, line, cudaGetErrorString( err) );
        exit(-1);
    }
}
inline void __cutilCheckMsg( const char *errorMessage, const char *file, const int line )
{
    cudaError_t err = cudaGetLastError();
    if( cudaSuccess != err) {
        fprintf(stderr, "cutilCheckMsg() CUTIL CUDA error: %s in file <%s>, line %i : %s.\n",
                errorMessage, file, line, cudaGetErrorString( err) );
        exit(-1);
    }
#ifdef _DEBUG
    err = cudaThreadSynchronize();
    if( cudaSuccess != err) {
        fprintf(stderr, "cutilCheckMsg cudaThreadSynchronize error: %s in file <%s>, line %i : %s.\n",
                errorMessage, file, line, cudaGetErrorString( err) );
        exit(-1);
    }
#endif
}
class CcudaKernel
{
public:
	CcudaKernel(void);
	~CcudaKernel(void);

	void DoKernel(dim3 gridD, dim3 blockD, int* g_data);
	void DoKernel2(dim3 gridD, dim3 blockD, int2* g_data);
	void launchLandKernel(float4* pos, unsigned int mesh_width, 
						 unsigned int mesh_height);
	void computeGold(char* reference, char* idata, const unsigned int len);
	void computeGold2(int2* reference, int2* idata, const unsigned int len);
	void runTest(const int argc, const char** argv, char* data, int2* data_int2, unsigned int len);

	void loadtotexture(cudaArray* cu_array, cudaChannelFormatDesc channelDesc);
	void unbindTexture();

public:
	bool success;

};
