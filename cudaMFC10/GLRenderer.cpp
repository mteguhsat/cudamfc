#include "StdAfx.h"
#include "GLRenderer.h"

// defines
#define MAXR(A,B) ((A) > (B) ? (A) : (B))
#define MINR(A,B) ((A) < (B) ? (A) : (B))

CGLRenderer::CGLRenderer(void)
{
	m_bIsMaximized = false;
	vertshaderland = 0;
	fragshaderland = 0;
	rotateX = 20.0f;
	rotateY = 125.0f;
	translateX = 0.0f;
	translateY = 0.0f;
	translateZ = -6.0f;
	drawPoints = true;
	wireFrame = false;

}

CGLRenderer::~CGLRenderer(void)
{
}

/************************************************************************
 * CreateGLContext() - setup OpenGL context
 ************************************************************************/
bool CGLRenderer::CreateGLContext(CDC* pDC)
{
	
	GLuint PixelFormat; 
	static PIXELFORMATDESCRIPTOR pfd = { 
		sizeof(PIXELFORMATDESCRIPTOR),    // pfd
		1,                                // 
		PFD_DRAW_TO_WINDOW |              //  
		PFD_SUPPORT_OPENGL |              // OpenGL 
		PFD_DOUBLEBUFFER,                 //  
		PFD_TYPE_RGBA,                    // RGBA 
		24,                               // 24
		0, 0, 0, 0, 0, 0,                 // 
		1,                                // 
		0,                                // 
		0,                                // 
		0, 0, 0, 0,                       // 
		32,                               // 32 
		8,                                // 
		0,                                // 
		PFD_MAIN_PLANE,                   // 
		0,                                // 
		0, 0, 0                           // 
	};

	// Did Windows Find A Matching Pixel Format?
	if (!(PixelFormat=ChoosePixelFormat(pDC->m_hDC,&pfd)))  
	{       
		::MessageBox(NULL,_T("Can't Find A Suitable PixelFormat."),_T("ERROR"),MB_OK|MB_ICONEXCLAMATION );
		return false;
	}

	// Are We Able To Set The Pixel Format?
	if(!SetPixelFormat(pDC->m_hDC,PixelFormat,&pfd))   
	{
		::MessageBox(NULL,_T("Can't Set The PixelFormat."),_T("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return false;    
	}

	// Are We Able To Get A Rendering Context?
	if (!(this->m_hrc=wglCreateContext(pDC->m_hDC))) 
	{
		::MessageBox(NULL,_T("Can't Create A GL Rendering Context."),_T("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return false;     
	}

	// Activate The Rendering Context
	if(!wglMakeCurrent(pDC->m_hDC,this->m_hrc))   
	{
		::MessageBox(NULL,_T("Can't Activate The GL Rendering Context."),_T("ERROR"),MB_OK|MB_ICONEXCLAMATION);
		return false;       
	}

	// initiliaze OpenGL Extension
	initGLbufferObject(true);

	return true;
}

/************************************************************************
 * DrawScene() - render the scene
 ************************************************************************/
void CGLRenderer::DrawScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//--------------------------------		

    float4 *dptr;
	// map OpenGL buffer object for writing from CUDA
    cutilSafeCall(cudaGLMapBufferObject((void**)&dptr, vboland));

	// create and launch CUDA kernel treads.
	m_cudaKernel.launchLandKernel(dptr, meshW, meshH);

	// unmap OpenGL buffer object for writing from CUDA
    cutilSafeCall(cudaGLUnmapBufferObject(vboland));

	// clear display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set view matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(translateX, translateY, translateZ);
    glRotatef(rotateX, 1.0, 0.0, 0.0);
    glRotatef(rotateY, 0.0, 1.0, 0.0);

	// render a terrain
	renderland();

	// draw a cube
	//OnDrawObject();

	glFlush (); 
	SwapBuffers(pDC->m_hDC);
	//--------------------------------
	wglMakeCurrent(NULL, NULL);
}

/************************************************************************
 * Reshape() - reshape
 ************************************************************************/
void CGLRenderer::Reshape(UINT nType, CDC *pDC, int w, int h)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------
	glViewport (0, 0, (GLsizei) w, (GLsizei) h); 

	// Projection view
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	// Set our current view perspective
	gluPerspective(35.0f, (float)w / (float)h, 0.01f, 2000.0f);

	// Model view
	glMatrixMode(GL_MODELVIEW);
	//---------------------------------
	wglMakeCurrent(NULL, NULL);
}

/************************************************************************
 * DestroyScene() - destroy scene when exit
 ************************************************************************/
void CGLRenderer::DestroyScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//--------------------------------
	m_fileHandler.cleanUpEx();

	m_cudaKernel.unbindTexture();
    cutilSafeCall(cudaFreeArray(cu_array));
	cleanUpBufferObject();
	cudaThreadExit();

	wglMakeCurrent(NULL,NULL); 
	//--------------------------------
	if(m_hrc) 
	{
		wglDeleteContext(m_hrc);
		m_hrc = NULL;
	}
}

/************************************************************************
 * PrepareScene() - preparing scene
 ************************************************************************/
void CGLRenderer::PrepareScene(CDC *pDC)
{
	wglMakeCurrent(pDC->m_hDC, m_hrc);
	//---------------------------------

	glShadeModel(GL_SMOOTH);       // Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);     // Black Background
	glClearDepth(1.0f);         // Depth Buffer Setup

    // load GLSL shader
	shaderland  = loadGLSLProgram(vertshaderland, fragshaderland);

	// Turn on backface culling
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);       // Enables Depth Testing
	glDepthFunc(GL_LEQUAL);        // The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really Nice Perspective Calculations

	setVBO();
	//---------------------------------
	wglMakeCurrent(NULL, NULL);
}

/************************************************************************
 * OnMouseMove() - mouse action
 ************************************************************************/
void CGLRenderer::OnMouseMove(UINT nFlags, CPoint point)
{
	int diffX = (int)(point.x - m_fLastX);
	int diffY = (int)(point.y - m_fLastY);
	m_fLastX  = (float)point.x;
	m_fLastY  = (float)point.y;

	// Left mouse button - rotate object
	if (nFlags & MK_LBUTTON)
	{
		rotateX += (float)0.5f * diffY;

		if ((rotateX > 360.0f) || (rotateX < -360.0f))
		{
			rotateX = 0.0f;
		}

		rotateY += (float)0.5f * diffX;

		if ((rotateY > 360.0f) || (rotateY < -360.0f))
		{
			rotateY = 0.0f;
		}
	}

	// Right mouse button - zoom in/out
	else if (nFlags & MK_RBUTTON)
	{
		translateZ += (float)0.1f * diffY;
	}

	// Middle mouse button - move object
	else if (nFlags & MK_MBUTTON)
	{
		translateX += (float)0.05f * diffX;
		translateY -= (float)0.05f * diffY;
	}
}

/************************************************************************
 * OnDrawObject() - draw an object (cube)
 ************************************************************************/
void CGLRenderer::OnDrawObject(void)
{
	// Wireframe Mode
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glBegin(GL_QUADS);
		// Front Side
		glVertex3f( 1.0f,  1.0f, 1.0f);
		glVertex3f(-1.0f,  1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, -1.0f, 1.0f);

		// Back Side
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f,  1.0f, -1.0f);
		glVertex3f( 1.0f,  1.0f, -1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);

		// Top Side
		glVertex3f( 1.0f, 1.0f,  1.0f);
		glVertex3f( 1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, 1.0f,  1.0f);

		// Bottom Side
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f, -1.0f,  1.0f);
		glVertex3f(-1.0f, -1.0f,  1.0f);

		// Right Side
		glVertex3f( 1.0f,  1.0f,  1.0f);
		glVertex3f( 1.0f, -1.0f,  1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f,  1.0f, -1.0f);

		// Left Side
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f,  1.0f);
		glVertex3f(-1.0f,  1.0f,  1.0f);
		glVertex3f(-1.0f,  1.0f, -1.0f);
	glEnd();
}

/************************************************************************
 * initInput() - get data from input file
 ************************************************************************/
void CGLRenderer::initInput(void)
{
	m_fileHandler.setFileNameEx("data//acehland80x180.dat");
	m_fileHandler.readInputFileEx();
	cudaSetDevice( cutGetMaxGflopsDeviceId() );
	/* loading input data */
	loadData();
}

/************************************************************************
 * loadData() - load data to GPU memory
 ************************************************************************/
void CGLRenderer::loadData(void)
{
	unsigned int size = meshW * meshH * sizeof(float);
	m_fileHandler.landdata = (float*)malloc(size);
	int k = 0;
	for(int i = 0; i < m_fileHandler.nrow; i++){
		for(int j = 0; j < m_fileHandler.ncolumn; j++){
			m_fileHandler.landdata[k] = m_fileHandler.land[i][j];									
			k++;
		}
	}

	// allocate array and copy data
    channelDesc = cudaCreateChannelDesc<float>();
	cutilSafeCall( cudaMallocArray( &cu_array, &channelDesc, 
								   meshW, meshH ));
	cutilSafeCall( cudaMemcpyToArray( cu_array, 0, 0, m_fileHandler.landdata, 
									 size, cudaMemcpyHostToDevice));		

	// load data to GPU as a texture
	m_cudaKernel.loadtotexture(cu_array, channelDesc);

}

/************************************************************************
 * initGLbufferObject() - Initialize GL Extension
 ************************************************************************/
bool CGLRenderer::initGLbufferObject(bool bFBODisplay)
{
	// get input data
	initInput();

	// get shader files
	vertshaderland = sdkFindFilePath("land.vert", NULL);
    fragshaderland = sdkFindFilePath("land.frag", NULL);
    if (vertshaderland == 0 || fragshaderland == 0) {
		MessageBox( NULL, "Error finding shaderland files!\n", "ERROR", MB_OK | MB_ICONERROR);
        cudaThreadExit();
        exit(EXIT_FAILURE);
    }

    // initialize necessary OpenGL extensions
    glewInit();

	// check GL Extension's version
    if (! glewIsSupported("GL_VERSION_2_0 " )) 
	{
		MessageBox( NULL, "ERROR: Support for \
						  necessary OpenGL extensions missing.", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }

	// check FBO support
    if (bFBODisplay) {
        if (!glewIsSupported( "GL_VERSION_2_0 GL_ARB_fragment_program \
							 GL_EXT_framebuffer_object" )) 
		{
			MessageBox( NULL, "Error: failed to get minimal extensions \n \
							  This sample requires:\n \
							  OpenGL version 2.0\n \
							  GL_ARB_fragment_program\n \
							  GL_EXT_framebuffer_object\n", "ERROR", MB_OK | MB_ICONERROR);
            cleanUpBufferObject();
            exit(-1);
        }
    } else {
		if (!glewIsSupported( "GL_VERSION_1_5 GL_ARB_vertex_buffer_object \
							 GL_ARB_pixel_buffer_object" )) 
		{
			MessageBox( NULL, "Error: failed to get minimal extensions \n \
							  This sample requires:\n \
							  OpenGL version 1.5\n \
							  GL_ARB_vertex_buffer_object\n \
							  GL_ARB_pixel_buffer_object\n", "ERROR", MB_OK | MB_ICONERROR);
            cleanUpBufferObject();
		    exit(-1);
	    }
    }

    CUT_CHECK_ERROR_GL();
    return true;
}

/************************************************************************
 * cleanUpBufferObject() - clear and delete vbo
 ************************************************************************/
void CGLRenderer::cleanUpBufferObject(void)
{
    cutilSafeCall(cudaGLUnregisterBufferObject(vboland));
    deleteVBO(&vboland);
}

/************************************************************************
 * deleteVBO() - delete vbo
 ************************************************************************/
void CGLRenderer::deleteVBO(GLuint* vbo)
{
    glDeleteBuffers(1, vbo);
    *vbo = 0;
}

/************************************************************************
 * loadGLSLProgram() - create shader program and load it
 ************************************************************************/
GLuint CGLRenderer::loadGLSLProgram(const char * vertFileName, const char * fragFileName)
{
    GLint linked;
    GLuint program;

    program = glCreateProgram();
    if (!attachShader(program, GL_VERTEX_SHADER, vertFileName)) {
        glDeleteProgram(program);
		MessageBox( NULL, "Couldn't attach vertex shader from file!\n", "ERROR", MB_OK | MB_ICONERROR);
        return 0;
    }

    if (!attachShader(program, GL_FRAGMENT_SHADER, fragFileName)) {
        glDeleteProgram(program);
        MessageBox( NULL, "Couldn't attach fragment shader from file!\n", "ERROR", MB_OK | MB_ICONERROR);
        return 0;
    }

    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (!linked) {
        glDeleteProgram(program);
        char temp[256];
        glGetProgramInfoLog(program, 256, 0, temp);
		MessageBox( NULL, "Failed to link program!\n", "ERROR", MB_OK | MB_ICONERROR);
        return 0;
    }

    return program;
}

/************************************************************************
 * attachShader() - attach shader file to shader program
 ************************************************************************/
int CGLRenderer::attachShader(GLuint prg, GLenum type, const char * name)
{
    GLuint shader;
    FILE * fp;
    int size, compiled;
    char * src;

    fp = fopen(name, "rb");
    if (!fp) return 0;

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    src = (char*)malloc(size);

    fseek(fp, 0, SEEK_SET);
    fread(src, sizeof(char), size, fp);
    fclose(fp);

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const char**)&src, (const GLint*)&size);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, (GLint*)&compiled);
    if (!compiled) {
        char log[2048];
        int len;

        glGetShaderInfoLog(shader, 2048, (GLsizei*)&len, log);
		MessageBox( NULL, log, "ERROR", MB_OK | MB_ICONERROR);
        glDeleteShader(shader);
        return 0;
    }
    free(src);
        
    glAttachShader(prg, shader);
    glDeleteShader(shader);

    return 1;
}

/************************************************************************
 * setVBO() - set VBO
 ************************************************************************/
void CGLRenderer::setVBO(void)
{
	// create VBO land
    createVBO(&vboland, meshW*meshH*4*sizeof(float));
	// register vbo to CUDA
    cutilSafeCall(cudaGLRegisterBufferObject(vboland));
	// create a mesh index buffer
    createMeshIndexBuffer(&indexvboland, meshW, meshH);
}

/************************************************************************
 * createVBO() - Create VBO
 ************************************************************************/
void CGLRenderer::createVBO(GLuint* vbo, int size)
{
    // create buffer object
    glGenBuffers(1, vbo);
    glBindBuffer(GL_ARRAY_BUFFER, *vbo);
    glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    CUT_CHECK_ERROR_GL();
}

/************************************************************************
 * createMeshIndexBuffer() - create a mesh index buffer
 ************************************************************************/
void CGLRenderer::createMeshIndexBuffer(GLuint* id, int w, int h)
{
    int size = ((w*2)+2)*(h-1)*sizeof(GLuint);

    // create index buffer
    glGenBuffersARB(1, id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *id);
    glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER, size, 0, GL_STATIC_DRAW);

    // fill with indices for rendering mesh as triangle strips
    GLuint *indices = (GLuint *) glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, 
											 GL_WRITE_ONLY);
    if (!indices) {
        return;
    }

    for(int y=0; y<h-1; y++) {
        for(int x=0; x<w; x++) {
            *indices++ = y*w+x;
            *indices++ = (y+1)*w+x;
        }
        // start new strip with degenerate triangle
		*indices++ = (y+1)*w+(w-1);
		*indices++ = (y+1)*w;
    }

    glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

/************************************************************************
 * renderland() - Render terrain
 ************************************************************************/
void CGLRenderer::renderland(void)
{
    // render from the vboland
    glBindBuffer(GL_ARRAY_BUFFER, vboland);
    glVertexPointer(4, GL_FLOAT, 0, 0);
	glEnableClientState(GL_VERTEX_ARRAY);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glLoadIdentity();
	//lighting();
    glUseProgram(shaderland);
    
    // Set default uniform variables parameters for the vertex shader
    GLuint uniHeightScale, uniChopiness, uniSize;

    uniHeightScale = glGetUniformLocation(shaderland, "heightScale");
    glUniform1f(uniHeightScale, 0.5f);

    uniChopiness   = glGetUniformLocation(shaderland, "chopiness");
    glUniform1f(uniChopiness, 1.0f);

    uniSize        = glGetUniformLocation(shaderland, "size");
    glUniform2f(uniSize, (GLfloat)meshW, (GLfloat)meshH);

    // Set default uniform variables parameters for the pixel shader
    GLuint uniDeepColor, uniShallowColor, uniSkyColor, uniLightDir;

    uniDeepColor = glGetUniformLocation(shaderland, "deepColor");
    glUniform4f(uniDeepColor, 0.0f, 1.0f, 0.0f, 1.0f);

    uniShallowColor = glGetUniformLocation(shaderland, "shallowColor");
    glUniform4f(uniShallowColor, 0.1f, 0.4f, 0.2f, 1.0f);

    uniSkyColor = glGetUniformLocation(shaderland, "skyColor");
    glUniform4f(uniSkyColor, 0.0f, 1.0f, 0.0f, 1.0f);

    uniLightDir = glGetUniformLocation(shaderland, "lightDir");
    glUniform3f(uniLightDir, 0.0f, 1.0f, 0.0f);
    // end of uniform settings

	glColor3f(0.0, 1.0, 0.0);
	if (drawPoints) {
		glDrawArrays(GL_POINTS, 0, meshW * meshH);		
	}else{
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexvboland);

        glPolygonMode(GL_FRONT_AND_BACK, wireFrame ? GL_LINE : GL_FILL);
            glDrawElements(GL_TRIANGLE_STRIP,((meshW*2)+2)*(meshH-1),
						   GL_UNSIGNED_INT, 0);        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
    glDisableClientState(GL_VERTEX_ARRAY);
	glUseProgram(0);
}
