// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "cudaMFC10.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};


// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	className = "class CMainFrame";
	// TODO: add member initialization code here
}

CMainFrame::~CMainFrame()
{
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
/*	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
*/
	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to be dockable
/*	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
*/
	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}


// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers




BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// TODO: Add your specialized code here and/or call the base class
	CRect windRect;
	this->GetWindowRect(&windRect);
	int wRow = 250;

		// Create the main splitter with 1 row and 2 columns
	if ( !m_mainSplitter.CreateStatic( this, 1, 2 ) )
	{
		MessageBox( "Error setting up m_mainSplitter", "ERROR", MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// The views for each pane must be created 
	if ( !m_mainSplitter.CreateView( 0, 0, RUNTIME_CLASS(CsideBar),
		CSize(185, windRect.Height()), pContext ) )
	{
		MessageBox( "Error setting up splitter view", "ERROR", MB_OK | MB_ICONERROR );
		return FALSE;
	}

	// The views for each pane must be created 
	if ( !m_mainSplitter.CreateView( 0, 1, RUNTIME_CLASS(CwhiteBoard),
		CSize(windRect.Width()/2, windRect.Height()), pContext ) )
	{
		MessageBox( "Error setting up splitter view", "ERROR", MB_OK | MB_ICONERROR );
		return FALSE;
	}

	return TRUE;//CFrameWnd::OnCreateClient(lpcs, pContext);
}

// get sideBar pane
CsideBar* CMainFrame::getPane00(void)
{
	return (CsideBar*)m_mainSplitter.GetPane(0,0);
}

// get whiteBoard pane
CwhiteBoard* CMainFrame::getPane01(void)
{
	return (CwhiteBoard*)m_mainSplitter.GetPane(0,1);
}
