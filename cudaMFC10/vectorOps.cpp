#include "StdAfx.h"
#include "vectorOps.h"

/**************
* dot product *
***************/
double dotProduct(const Vec3d U, const Vec3d V)
{
	return U.x * V.x + U.y * V.y + U.z * V.z;
}

/**************
* unit normal *
***************/
void unitnorm(const Vec3d U, Vec3d V)
{
	double u = sqrt(dotProduct(U,U));
	V.x = U.x/u;
	V.y = U.y/u;
	V.z = U.z/u;
}

void vectorize(const Point3d P1, const Point3d P2, Vec3d Vt)
{
	Vt.x = P2.a - P1.a;
	Vt.y = P2.b - P1.b;
	Vt.z = P2.c - P1.c;
}
