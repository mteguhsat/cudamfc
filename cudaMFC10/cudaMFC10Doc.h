// cudaMFC10Doc.h : interface of the CcudaMFC10Doc class
//


#pragma once
#include "fileHandler.h"
#include <vector>
using namespace std; 

class CcudaMFC10Doc : public CDocument
{
protected: // create from serialization only
	CcudaMFC10Doc();
	DECLARE_DYNCREATE(CcudaMFC10Doc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CcudaMFC10Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	// content
	vector<CString> cudaTesting;
	vector<CString> GPUprop;
	CfileHandler	mfileHandler;

	vector<CString> dataHeader;
	vector< vector<double> > dataBody;

	CString contentText;
	CString className;
	CString fileName;
	CString folderPath;
	CString fileExt;
	CString pathName;
	afx_msg void OnFileSaveAs();
	afx_msg void OnFileSave();
	afx_msg void OnFileOpen();
};


