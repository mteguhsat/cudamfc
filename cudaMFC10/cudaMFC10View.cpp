// cudaMFC10View.cpp : implementation of the CcudaMFC10View class
//

#include "stdafx.h"
#include "cudaMFC10.h"

#include "cudaMFC10View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CcudaMFC10View

IMPLEMENT_DYNCREATE(CcudaMFC10View, CView)

BEGIN_MESSAGE_MAP(CcudaMFC10View, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CcudaMFC10View construction/destruction

CcudaMFC10View::CcudaMFC10View()
{
	// TODO: add construction code here
	this->className= "class CcudaMFC10View";
}

CcudaMFC10View::~CcudaMFC10View()
{
}

BOOL CcudaMFC10View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CcudaMFC10View drawing


// CcudaMFC10View printing

BOOL CcudaMFC10View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CcudaMFC10View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CcudaMFC10View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CcudaMFC10View diagnostics

#ifdef _DEBUG
void CcudaMFC10View::AssertValid() const
{
	CView::AssertValid();
}

void CcudaMFC10View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CcudaMFC10Doc* CcudaMFC10View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CcudaMFC10Doc)));
	return (CcudaMFC10Doc*)m_pDocument;
}
#endif //_DEBUG


// CcudaMFC10View message handlers

