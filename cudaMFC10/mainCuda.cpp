#include "StdAfx.h"
#include "mainCuda.h"

CmainCuda::CmainCuda(void)
{
	status = false;
}

CmainCuda::~CmainCuda(void)
{
}

// execute CUDA
void CmainCuda::runCuda(void)
{
	int argc = NULL;
	char** argv = NULL;
    // input data
    int len = 16;
    // the data has some zero padding at the end so that the size is a multiple of
    // four, this simplifies the processing as each thread can process four
    // elements (which is necessary to avoid bank conflicts) but no branching is
    // necessary to avoid out of bounds reads
    char str[] = { 82, 111, 118, 118, 121, 42, 97, 121, 124, 118, 110, 56,
                   10, 10, 10, 10};

    // Use int2 showing that CUDA vector types can be used in cpp code
	CString tmp;
	inpstr = "";
    for( int i = 0; i < len; i++ )
    {
        i2[i].x = str[i];
        i2[i].y = 10;
		tmp.Format("%c", str[i]-10);
		inpstr += tmp;
    }

	cudaGetDeviceProperties(&deviceProp, 0);
	cudartVersion = CUDART_VERSION;
	if (cudartVersion >= 2020) {
		cudaDriverGetVersion(&driverVersion);
		cudaRuntimeGetVersion(&runtimeVersion);
	}

    // run the device part of the program
	m_Cuda.runTest(argc,(const char**)argv, str, i2, len);

	outstr = "";
	for( int i = 0; i < len; i++ )
	{
		tmp.Format("%c", i2[i].x);
		outstr += tmp;
	}

	success = m_Cuda.success ? "CUDA PASSED" :"CUDA FAILED" ;

}
