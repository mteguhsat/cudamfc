// cudaMFC10View.h : interface of the CcudaMFC10View class
//


#pragma once
#include "cudaMFC10Doc.h"

class CcudaMFC10View : public CView
{
protected: // create from serialization only
	CcudaMFC10View();
	DECLARE_DYNCREATE(CcudaMFC10View)

// Attributes
public:
	CcudaMFC10Doc* GetDocument() const;

// Operations
public:
	CString className;
// Overrides
public:
	virtual void OnDraw(CDC* pDC){}  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CcudaMFC10View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
};

#ifndef _DEBUG  // debug version in cudaMFC10View.cpp
inline CcudaMFC10Doc* CcudaMFC10View::GetDocument() const
   { return reinterpret_cast<CcudaMFC10Doc*>(m_pDocument); }
#endif

