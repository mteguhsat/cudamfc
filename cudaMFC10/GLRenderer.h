#pragma once
#include "cudaKernel.h"
#include "fileHandler.h"
#include "helper_string.h"

#define CUT_CHECK_ERROR_GL()                                               \
	if( false == cutCheckErrorGL( __FILE__, __LINE__)) {                  \
	exit(EXIT_FAILURE);                                                  \
	}

inline bool cutCheckErrorGL( const char* file, const int line) 
{
	bool ret_val = true;

	// check for error
	GLenum gl_error = glGetError();
	if (gl_error != GL_NO_ERROR) 
	{
		fprintf(stderr, "GL Error in file '%s' in line %d :\n", file, line);
		fprintf(stderr, "%s\n", gluErrorString(gl_error));
		ret_val = false;
	}
	return ret_val;
}

// This function returns the best GPU (with maximum GFLOPS)
inline int cutGetMaxGflopsDeviceId()
{
	int device_count = 0;
	cudaGetDeviceCount( &device_count );

	cudaDeviceProp device_properties;
	int max_gflops_device = 0;
	int max_gflops = 0;
	
	int current_device = 0;
	cudaGetDeviceProperties( &device_properties, current_device );
	max_gflops = device_properties.multiProcessorCount * device_properties.clockRate;
	++current_device;

	while( current_device < device_count )
	{
		cudaGetDeviceProperties( &device_properties, current_device );
		int gflops = device_properties.multiProcessorCount * device_properties.clockRate;
		if( gflops > max_gflops )
		{
			max_gflops        = gflops;
			max_gflops_device = current_device;
		}
		++current_device;
	}

	return max_gflops_device;
}

class CGLRenderer
{
public:
	CGLRenderer(void);
	~CGLRenderer(void);
	bool CreateGLContext(CDC* pDC);			// Creates OpenGL Rendering Context
	void DrawScene(CDC* pDC);				// Draws the scene
	void Reshape(UINT nType, CDC* pDC, int w, int h);	// Changing viewport
	void DestroyScene(CDC* pDC);			// Cleanup
	void PrepareScene(CDC* pDC);			// Scene preparation stuff
	void OnMouseMove(UINT nFlags, CPoint point);

protected:
	HGLRC	 m_hrc;					// OpenGL Rendering Context 

public:
	CcudaKernel		m_cudaKernel;
	CfileHandler	m_fileHandler;

	// View information variables
	float	 m_fLastX;
	float	 m_fLastY;
	bool	 m_bIsMaximized;
	float	rotateX;
	float	rotateY;
	float	translateX;
	float	translateY;
	float	translateZ;
	bool	drawPoints;
	bool	wireFrame;
	
	// data input variables
	//ifstream infile;
	//int ncolumn;	/*equal to meshW*/
	//int nrow;		/*equal to meshH*/
	//int dx;
	//int dy;
	//float minland;
	//float maxland;
	//float * landdata;
	///* array variables */
	//vector< vector<float> > land;			//terrain data

	// data array
	cudaArray* cu_array;
	cudaChannelFormatDesc channelDesc;
	
	// vboland variables
	GLuint vboland;
	GLuint indexvboland;
	GLuint shaderland;
	char* vertshaderland;
	char* fragshaderland;
		
	static const unsigned int meshW = 80;//8;//
	static const unsigned int meshH = 180;//16;//


	void OnDrawObject(void);
	bool initGLbufferObject(bool bFBODisplay);
	void cleanUpBufferObject(void);
	void deleteVBO(GLuint* vbo);
	GLuint loadGLSLProgram(const char * vertFileName, const char * fragFileName);
	int attachShader(GLuint prg, GLenum type, const char * name);
	void setVBO(void);
	void createVBO(GLuint* vbo, int size);
	void createMeshIndexBuffer(GLuint* id, int w, int h);
	void initInput(void);
	void loadData(void);
	void renderland(void);
};
