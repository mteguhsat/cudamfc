// whiteBoard.cpp : implementation file
//

#include "stdafx.h"
#include "cudaMFC10.h"
#include "whiteBoard.h"


// CwhiteBoard

IMPLEMENT_DYNCREATE(CwhiteBoard, CcudaMFC10View)

CwhiteBoard::CwhiteBoard()
{
	className = "class CwhiteBoard";
}

CwhiteBoard::~CwhiteBoard()
{
}

BEGIN_MESSAGE_MAP(CwhiteBoard, CcudaMFC10View)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CwhiteBoard drawing

// CwhiteBoard diagnostics

#ifdef _DEBUG
void CwhiteBoard::AssertValid() const
{
	CcudaMFC10View::AssertValid();
}

#ifndef _WIN32_WCE
void CwhiteBoard::Dump(CDumpContext& dc) const
{
	CcudaMFC10View::Dump(dc);
}
#endif
#endif //_DEBUG


// CwhiteBoard message handlers


int CwhiteBoard::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CcudaMFC10View::OnCreate(lpCreateStruct) == -1)
		return -1;

	CDC* pDC = GetDC();
	m_glRenderer.CreateGLContext(pDC);
	ReleaseDC(pDC);

	return 0;
}

void CwhiteBoard::OnDraw(CDC* pDC)
{
	CcudaMFC10Doc* pDoc = GetDocument();
	m_glRenderer.DrawScene(pDC);
}

void CwhiteBoard::OnSize(UINT nType, int cx, int cy)
{
	CcudaMFC10View::OnSize(nType, cx, cy);
	
	CDC* pDC = GetDC();
	m_glRenderer.Reshape(nType, pDC, cx, cy);
	ReleaseDC(pDC);
}

void CwhiteBoard::OnDestroy()
{
	CcudaMFC10View::OnDestroy();

	CDC* pDC = GetDC();
	m_glRenderer.DestroyScene(pDC);
	ReleaseDC(pDC);
	DeleteDC(pDC->m_hDC);
}

BOOL CwhiteBoard::OnEraseBkgnd(CDC* pDC)
{
	return true;
}

void CwhiteBoard::OnInitialUpdate()
{
	CcudaMFC10View::OnInitialUpdate();

	CDC* pDC = GetDC();
	m_glRenderer.PrepareScene(pDC);
	ReleaseDC(pDC);	

}

void CwhiteBoard::OnMouseMove(UINT nFlags, CPoint point)
{
	CcudaMFC10View::OnMouseMove(nFlags, point);

	CDC* pDC = GetDC();
	m_glRenderer.OnMouseMove(nFlags, point);
	OnDraw(pDC);
	ReleaseDC(pDC);	
}

BOOL CwhiteBoard::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= ( WS_CLIPCHILDREN | WS_CLIPSIBLINGS | CS_OWNDC ); 
	return CcudaMFC10View::PreCreateWindow(cs);
}


