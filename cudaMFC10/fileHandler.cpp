#include "StdAfx.h"
#include "fileHandler.h"

// defines
#define MAXR(A,B) ((A) > (B) ? (A) : (B))
#define MINR(A,B) ((A) < (B) ? (A) : (B))

CfileHandler::CfileHandler(void)
{
}

CfileHandler::~CfileHandler(void)
{
}

void CfileHandler::setFileNameEx(string filename)
{
	flnameEx = filename;
}

string CfileHandler::getFileNameEx(void)
{
	return flnameEx;
}

void CfileHandler::readInputFileEx(void)
{
	if (flnameEx.empty()){
		MessageBox( NULL, "Cannot open data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		return;
	}

	infileEx.open(flnameEx.c_str());
	if (!infileEx.is_open()){
		MessageBox( NULL, "Cannot open data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (!readHeaderFileEx()){
		MessageBox( NULL, "Cannot read header of data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (!readBodyFileEx()){
		MessageBox( NULL, "Cannot read data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	infileEx.close();
}

/************************************************************************
 * readHeaderFileEx() - Read the header of data input file
 ************************************************************************/
int CfileHandler::readHeaderFileEx(void)
{
	string strg;
	getline(infileEx, strg);

	char *cstr, *p;	
	cstr = new char [strg.size()+1];
	strcpy (cstr, strg.c_str());
	p=strtok(cstr,"\t");
	int j = 0;
	while (p!=NULL)
	{		
		switch (j)
		{
		case 0:
			ncolumn = atoi(p);
		case 1:
			nrow	= atoi(p);
		case 2:
			dx	= atoi(p);
		case 3:
			dy	= atoi(p);
		}
		j++;
		p=strtok(NULL,"\t");
	}
	delete [] cstr;	
	return 1;
}

/************************************************************************
 * readBodyFileEx() - Read the body of data input file
 ************************************************************************/
int CfileHandler::readBodyFileEx(void)
{
	minland = 99999;
	maxland = 0;
	while (!infileEx.eof()){

		vector<float> axis;
		float val;
		string strg;
		getline(infileEx, strg);

		char *cstr, *p;	
		cstr = new char [strg.size()+1];
		strcpy (cstr, strg.c_str());
		p=strtok(cstr,"\t");
		while (p!=NULL)
		{		
			val = (float)atof(p);
			minland = MINR(minland,val);
			maxland = MAXR(maxland,val);
			axis.push_back(val);
			p=strtok(NULL,"\t");
		}
		delete [] cstr;	

		land.push_back(axis);

	}
	return 1;
}

void CfileHandler::cleanUpEx(void)
{
	land.clear();			//terrain data
	free(landdata);
}

void CfileHandler::setDataFile(CString pathname)
{
	filenm = pathname;
}

CString CfileHandler::getDataFile(void)
{
	return filenm;
}

void CfileHandler::parseDataFile(vector<CString> * header, vector< vector<double> > * body)
{
	inpfile.open(filenm);
	if (!inpfile.is_open()){
		MessageBox( NULL, "Cannot open data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (!parseHeaderDataFile(header)){
		MessageBox( NULL, "Cannot read header of data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (!parseBodyDataFile(body)){
		MessageBox( NULL, "Cannot read data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	inpfile.close();
}

int CfileHandler::parseHeaderDataFile(vector<CString> * header)
{
	header->clear();
	string strg;
	getline(inpfile, strg);

	char *cstr, *p;	
	cstr = new char [strg.size()+1];
	strcpy (cstr, strg.c_str());
	p=strtok(cstr,"\t");
	int j = 0;
	while (p!=NULL)
	{		
		header->push_back(p);
		j++;
		p=strtok(NULL,"\t");
	}
	delete [] cstr;	
	return 1;
}

int CfileHandler::parseBodyDataFile(vector< vector<double> > * body)
{
	body->clear();
	while (!inpfile.eof()){
		vector<double> vtemp;
		double val;
		string strg;
		getline(inpfile, strg);

		char *cstr, *p;	
		cstr = new char [strg.size()+1];
		strcpy (cstr, strg.c_str());
		p=strtok(cstr,"\t");
		while (p!=NULL)
		{		
			val = atof(p);
			vtemp.push_back(val);
			p=strtok(NULL,"\t");
		}
		delete [] cstr;	

		body->push_back(vtemp);
	}
	return 1;
}

void CfileHandler::setSavedFileName(CString filename)
{
	svfilenm = filename;
}

void CfileHandler::saveDataToFile(vector<CString>* header, vector< vector<double> >* body)
{
	savedfile.open(svfilenm);
	if (!savedfile.is_open()){
		MessageBox( NULL, "Cannot open data file.\n", "ERROR", MB_OK | MB_ICONERROR);
		exit(1);
	}
	writeHeaderToSavedFile(header);
	writeBodyToSavedFile(body);
	savedfile.close();
}

void CfileHandler::writeHeaderToSavedFile(vector<CString>* header)
{
	vector<CString>::iterator iter = header->begin();
	while (iter != header->end()){
		savedfile << *iter;
		iter++;
		if (iter != header->end())
			savedfile << "\t";
		else
			savedfile << "\n";
	}
}

void CfileHandler::writeBodyToSavedFile(vector< vector<double> >* body)
{
	vector< vector<double> >::iterator iter1 = body->begin();
	vector<double>::iterator iter2;
	while (iter1 != body->end()){
		iter2 = (*iter1).begin();
		while (iter2 != (*iter1).end()){
			savedfile << *iter2;
			iter2++;
			if (iter2 != (*iter1).end())
				savedfile << "\t";
			else
				savedfile << "\n";
		}
		iter1++;
	}
}
