// sideBar.cpp : implementation file
//

#include "stdafx.h"
#include "cudaMFC10.h"
#include "sideBar.h"
#include "MainFrm.h"


// CsideBar

IMPLEMENT_DYNCREATE(CsideBar, CFormView)

CsideBar::CsideBar()
	: CFormView(CsideBar::IDD)
{
}

CsideBar::~CsideBar()
{
}

void CsideBar::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LBOX, mlisBox);
}

BEGIN_MESSAGE_MAP(CsideBar, CFormView)
	ON_BN_CLICKED(IDC_BSTART, &CsideBar::OnBnClickedBstart)
END_MESSAGE_MAP()


// CsideBar diagnostics

#ifdef _DEBUG
void CsideBar::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CsideBar::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CsideBar message handlers


void CsideBar::OnBnClickedBstart()
{
	CMainFrame * pFrame = (CMainFrame *)AfxGetMainWnd();
	CwhiteBoard *pView = pFrame->getPane01();	
	CcudaMFC10Doc * pDoc = pView->GetDocument();
	ASSERT_VALID(pDoc);

	pDoc->GPUprop.clear();
	pDoc->cudaTesting.clear();

	m_mainCuda.runCuda();

	CString temp;
	temp.Format("\nDevice %d: \"%s\"\n", 0, m_mainCuda.deviceProp.name);
	pDoc->GPUprop.push_back(temp);
	mlisBox.AddString(temp);

	mlisBox.AddString("CUDA Properties");

	if (m_mainCuda.cudartVersion >= 2020) {
		temp.Format("  Driver Version: %d.%d\n", 
			m_mainCuda.driverVersion/1000, m_mainCuda.driverVersion%100);
		pDoc->GPUprop.push_back(temp);
		mlisBox.AddString(temp);

		temp.Format("  Runtime Version: %d.%d\n", 
			m_mainCuda.runtimeVersion/1000, m_mainCuda.runtimeVersion%100);
		pDoc->GPUprop.push_back(temp);
		mlisBox.AddString(temp);
	}

	temp.Format("  Capbly. Major rev. : %d\n", 
		m_mainCuda.deviceProp.major);
	pDoc->GPUprop.push_back(temp);
	mlisBox.AddString(temp);

	temp.Format("  Capbly. Minor rev. : %d\n", 
		m_mainCuda.deviceProp.minor);
	pDoc->GPUprop.push_back(temp);
	mlisBox.AddString(temp);

	temp.Format("  Global memory: %u Mbytes\n", 
		m_mainCuda.deviceProp.totalGlobalMem/(1024*1024));
	pDoc->GPUprop.push_back(temp);
	mlisBox.AddString(temp);

	if (m_mainCuda.cudartVersion >= 2000) {
		temp.Format("  Number of multiprocessors: %d\n", 
			m_mainCuda.deviceProp.multiProcessorCount);
		pDoc->GPUprop.push_back(temp);
		mlisBox.AddString(temp);

		temp.Format("  Total cores: %d\n", _ConvertSMVer2Cores(m_mainCuda.deviceProp.major, m_mainCuda.deviceProp.minor) * 
			m_mainCuda.deviceProp.multiProcessorCount);
		pDoc->GPUprop.push_back(temp);
		mlisBox.AddString(temp);
	}
	pDoc->GPUprop.push_back("\n");

	pDoc->cudaTesting.push_back("********[ CUDA testing ]**********");
	mlisBox.AddString("*****[ CUDA testing ]*******");

	pDoc->cudaTesting.push_back("Sent to kernel : "+m_mainCuda.inpstr);
	mlisBox.AddString("Sent to kernel : "+m_mainCuda.inpstr);

	pDoc->cudaTesting.push_back("Get from kernel : "+m_mainCuda.outstr);
	mlisBox.AddString("Get from kernel : "+m_mainCuda.outstr);

	pDoc->cudaTesting.push_back("\n");

	pDoc->cudaTesting.push_back(m_mainCuda.success);
	mlisBox.AddString(m_mainCuda.success);

	pDoc->cudaTesting.push_back("**********************************");
	mlisBox.AddString("*************************");

	//pView->OnDraw(pView->GetDC());

}
