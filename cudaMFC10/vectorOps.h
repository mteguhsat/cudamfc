#pragma once

class Vec3d;
class Point3d;

double dotProduct( Vec3d U, Vec3d V);
void unitnorm( Vec3d U, Vec3d V);
void vectorize(const Point3d P1, const Point3d P2, Vec3d Vt);

class Vec3d
{
public:
	Vec3d(void){
		x = 0;
		y = 0;
		z = 0;
	}
	~Vec3d(void){
	}
public:
	int x;
	int y;
	int z;
};

class Point3d
{
public:
	Point3d(void){
		a = 0;
		b = 0;
		c = 0;
	}
	~Point3d(void){
	}
public:
	int a;
	int b;
	int c;
};
