#pragma once
//#include <fstream>
//using namespace std;
#include <vector>
#include <string>
using namespace std;

class CfileHandler
{
public:
	CfileHandler(void);
	~CfileHandler(void);
	void setFileNameEx(string filename);
	string getFileNameEx(void);
	void readInputFileEx(void);
	void cleanUpEx(void);
protected:
	int readHeaderFileEx(void);
	int readBodyFileEx(void);

protected:
	ifstream	infileEx;
	string		flnameEx;

	ifstream	inpfile;
	CString		filenm;

	ofstream	savedfile;
	CString		svfilenm;

public:
	// data input variables
	int ncolumn;	/*equal to meshW*/
	int nrow;		/*equal to meshH*/
	int dx;
	int dy;
	float minland;
	float maxland;
	float * landdata;
	vector< vector<float> > land;			//terrain data

public:
	void setDataFile(CString pathname);
	CString getDataFile(void);
	void parseDataFile(vector<CString> * header, vector< vector<double> > * body);
	void saveDataToFile(vector<CString>* header, vector< vector<double> >* body);
protected:
	int parseHeaderDataFile(vector<CString> * header);
	int parseBodyDataFile(vector< vector<double> > * body);
	void writeHeaderToSavedFile(vector<CString>* header);
	void writeBodyToSavedFile(vector< vector<double> >* body);
public:
	void setSavedFileName(CString filename);
};
