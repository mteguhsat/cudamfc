// cudaMFC10.h : main header file for the cudaMFC10 application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CcudaMFC10App:
// See cudaMFC10.cpp for the implementation of this class
//

class CcudaMFC10App : public CWinApp
{
public:
	CcudaMFC10App();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CcudaMFC10App theApp;