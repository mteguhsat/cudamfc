#include "cudaKernel.h"

// declare texture reference for 2D float texture
texture<float, 2, cudaReadModeElementType> tex;


__global__ void
kernel( int* g_data )
{
    // write data to global memory
    const unsigned int tid = threadIdx.x;
    int data = g_data[tid];

    // use integer arithmetic to process all four bytes with one thread
    // this serializes the execution, but is the simplest solutions to avoid 
    // bank conflicts for this very low number of threads
    // in general it is more efficient to process each byte by a separate thread,
    // to avoid bank conflicts the access pattern should be 
    // g_data[4 * wtid + wid], where wtid is the thread id within the half warp 
    // and wid is the warp id
    // see also the programming guide for a more in depth discussion.
    g_data[tid] = ((((data <<  0) >> 24) - 10) << 24)
                | ((((data <<  8) >> 24) - 10) << 16)
                | ((((data << 16) >> 24) - 10) <<  8)
                | ((((data << 24) >> 24) - 10) <<  0);
}

///////////////////////////////////////////////////////////////////////////////
//! Demonstration that int2 data can be used in the cpp code
//! @param g_odata  memory to process (in and out)
///////////////////////////////////////////////////////////////////////////////
__global__ void
kernel2( int2* g_data )
{
    // write data to global memory
    const unsigned int tid = threadIdx.x;
    int2 data = g_data[tid];

    // use integer arithmetic to process all four bytes with one thread
    // this serializes the execution, but is the simplest solutions to avoid 
    // bank conflicts for this very low number of threads
    // in general it is more efficient to process each byte by a separate thread,
    // to avoid bank conflicts the access pattern should be 
    // g_data[4 * wtid + wid], where wtid is the thread id within the half warp 
    // and wid is the warp id
    // see also the programming guide for a more in depth discussion.
    g_data[tid].x = data.x - data.y;
}

/************************************************************************
 * landKernel() - 
 ************************************************************************/
__global__ void landKernel(float4* pos, unsigned int width, 
							  unsigned int height)
{
	unsigned int i = __mul24(blockIdx.x, blockDim.x) + threadIdx.x;
    unsigned int j = __mul24(blockIdx.y, blockDim.y) + threadIdx.y;
	int index = j*width + i;

    // calculate xz coordinates
    float x = (i / (float) width)*2.0f - 1.0f;
    float z = (j / (float) height)*4.0f - 2.0f;
 
    // read texture
    float y = (tex2D(tex, i, j))/200.0f;

    // write output vertex ---> uncoalesced memory;
    pos[index] = make_float4(x, y, z, 1.0f);
}

CcudaKernel::CcudaKernel(void)
{
	success = false;
}

CcudaKernel::~CcudaKernel(void)
{
}

void CcudaKernel::DoKernel(dim3 gridD, dim3 blockD, int* g_data)
{
	kernel<<< gridD, blockD >>>(g_data);
}

void CcudaKernel::DoKernel2(dim3 gridD, dim3 blockD, int2* g_data)
{
    kernel2<<< gridD, blockD >>>(g_data);
}

void CcudaKernel::launchLandKernel(float4* pos, unsigned int mesh_width, 
						 unsigned int mesh_height)
{
    // execute the kernel
    dim3 block(16,6,1);//(16, 16, 1);//
    dim3 gridland(mesh_width / block.x, mesh_height / block.y, 1);
    landKernel<<<gridland, block>>>(pos, mesh_width, mesh_height);
	//printf("here\n");
}
////////////////////////////////////////////////////////////////////////////////
// declaration, forward
//extern "C" 
void CcudaKernel::computeGold(char* reference, char* idata, const unsigned int len)
{
    for(unsigned int i = 0; i < len; ++i)
        reference[i] = idata[i] - 10;
}

//extern "C" 
void CcudaKernel::computeGold2(int2* reference, int2* idata, const unsigned int len)
{
    for(unsigned int i = 0; i < len; ++i)
    {
        reference[i].x = idata[i].x - idata[i].y;
        reference[i].y = idata[i].y;
    }
}

void CcudaKernel::runTest(const int argc, const char** argv, char* data, int2* data_int2, unsigned int len)
{
	//use command-line specified CUDA device, otherwise use device with highest Gflops/s
	//if( cutCheckCmdLineFlag(argc, (const char**)argv, "device") )
	//	cutilDeviceInit(argc, (char**)argv);
	//else
	//	cudaSetDevice( cutGetMaxGflopsDeviceId() );

    const unsigned int num_threads = len / 4;
    //cutilCondition(0 == (len % 4));
	if (0 != (len % 4)) exit(EXIT_FAILURE);
    const unsigned int mem_size = sizeof(char) * len;
    const unsigned int mem_size_int2 = sizeof(int2) * len;

    // allocate device memory
    char* d_data;
    cutilSafeCall(cudaMalloc((void**) &d_data, mem_size));
    // copy host memory to device
    cutilSafeCall(cudaMemcpy(d_data, data, mem_size,
                            cudaMemcpyHostToDevice) );
    // allocate device memory for int2 version
    int2* d_data_int2;
    cutilSafeCall(cudaMalloc((void**) &d_data_int2, mem_size_int2));
    // copy host memory to device
    cutilSafeCall(cudaMemcpy(d_data_int2, data_int2, mem_size_int2,
                            cudaMemcpyHostToDevice) );

    // setup execution parameters
    dim3 grid(1, 1, 1);
    dim3 threads(num_threads, 1, 1);
    dim3 threads2(len, 1, 1); // more threads needed fir separate int2 version
    // execute the kernel

	DoKernel(grid, threads, (int*) d_data);

	DoKernel2(grid, threads2, d_data_int2);

    // check if kernel execution generated and error
    cutilCheckMsg("Kernel execution failed");

    // compute reference solutions
    char* reference = (char*) malloc(mem_size);
    computeGold(reference, data, len);
    int2* reference2 = (int2*) malloc(mem_size_int2);
    computeGold2(reference2, data_int2, len);

    // copy results from device to host
    cutilSafeCall(cudaMemcpy(data, d_data, mem_size,
                            cudaMemcpyDeviceToHost));
    cutilSafeCall(cudaMemcpy(data_int2, d_data_int2, mem_size_int2,
                            cudaMemcpyDeviceToHost));

    // check result
    success = true;
    for(unsigned int i = 0; i < len; i++ )
    {
        if( reference[i] != data[i] || 
	    reference2[i].x != data_int2[i].x || 
	    reference2[i].y != data_int2[i].y)
            success = false;
    }

    // cleanup memory
    cutilSafeCall(cudaFree(d_data));
    cutilSafeCall(cudaFree(d_data_int2));
    free(reference);
    free(reference2);

//    cudaThreadExit();
}

void CcudaKernel::loadtotexture(cudaArray* cu_array, cudaChannelFormatDesc channelDesc)
{
    // set texture parameters
	// cudaAddressModeWrap --> in which case out-of-range 
	// texture coordinates are wrapped to the valid range
    //tex.addressMode[0] = cudaAddressModeWrap;
    //tex.addressMode[1] = cudaAddressModeWrap;
    tex.addressMode[0] = cudaAddressModeClamp;
    tex.addressMode[1] = cudaAddressModeClamp;
	// cudaFilterModePoint --> the returned value is the texel whose 
	// texture coordinates are the closest to the input texture coordinates
    tex.filterMode = cudaFilterModePoint;
    //tex.filterMode = cudaFilterModeLinear;
	// normalized --> access with normalized texture coordinates
    //tex.normalized = true;    
    tex.normalized = false;    
    // Bind the array to the texture
    //cudaBindTextureToArray( tex, cu_array, channelDesc);
    cudaBindTextureToArray(tex, cu_array);
}

void CcudaKernel::unbindTexture()
{
	cudaUnbindTexture(tex);
}
