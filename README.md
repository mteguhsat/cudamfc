# cudaMFC #

**Latest Test : VS2010 and CUDA 6.5**

### How to set up ###

1. Open the project in Visual Studio
2. Build > Build Solution or press F7
3. Copy the .dll files and data folder to the directory where the .exe file was built.

#### Comments and Questions ####

http://m.teguhsatria.com/cuda-mfc 

email: **awak[AT]teguhsatria[DOT]com** or **mteguhsat[AT]gmail[DOT]com**